<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Account Controller
 */
class Account extends SheepCode_Controller {

	function __construct()
  	{
	    parent::__construct();

      $this->load->model('Account_model');
			$this->load->model('Payment_model');
			$this->load->model('Customer_model');
			$this->load->model('CustomerGroup_model');


  	}



	public function create_form()
	{
		$group = $this->CustomerGroup_model->find_all();
    $this->render('account/createaccount_form',['title_page'=>'ค้นหา บัญชีลูกค้า','group'=>$group]);

	}
  public function createaccount_form()
  {

    $this->render('account/create_form',['title_page'=>'จัดการ บัญชีลูกค้า']);

  }


	public function paid(){

		if ($this->Payment_model->paid()) {
			$this->render_json(["result"=>true]);
		}else{
			$this->render_json(["result"=>false]);
		}


	}


	public function create_account(){
		$this->Account_model->create();
		$this->render_json(["result"=>"success"]);
	}


	public function isValidCustomer(){
		$count = $this->Account_model->checkUser();
		if ($count->found == 0) {
			$this->render_json(true);
		}else{
			$this->render_json(false);
		}

	}


   public function search(){
    $result_set = $this->Account_model->search();

    $response = [];

    foreach ($result_set as $key => $value) {
     array_push($response, [
        "name" => 'คุณ '.$value->customer_firstname ." , ". $value->customer_group_name,
        "id" => $value->customer_id,
        "fulldata" => $value
      ]);
    }
    $this->render_json($response);
  }
  public function datatable(){

      $result_set = $this->Account_model->find_all();

			foreach ($result_set as $key => $value) {
 		 		$value->customer_firstname  = $value->customer_firstname . '/' . $value->customer_group_name;
 		 		$str = explode("#",$value->account_name);
 		 		$value->account_name = $str[0];
 		}

      $this->render_json(['data'=>$result_set]);

    }

  public function show_account_detail(){
			$account_id = $this->input->get('account_id');

			$payments = $this->Payment_model->find_by_accountid($account_id) ;

			$sum_payment = 0 ;

			foreach ($payments as $key => $value) {
				$sum_payment += $value->payment_amount;
			}


			$data = [
				"customer" => $this->Customer_model->find_by_accountid($account_id) ,
				"account" => $this->Account_model->find_by_id($account_id),
				"payments" => $payments,
				"payments_sum" =>$sum_payment
			];

			//var_dump($data);

    $this->render('account/view_detail',['title_page'=>'จัดการ บัญชีลูกค้า','info'=>$data]);
  }

	public function update_account_status(){
		$this->Account_model->update_account_status();

		redirect('cus_account/show_account_detail?account_id=' . $this->input->get('account_id') ,'refresh');

	}

	public function delete_account(){

		$account_id = $this->input->post('account_id');

		if( $this->Payment_model->count_by_accountid($account_id)  < 1 ){

			$this->Account_model->delete_account($account_id);
			$this->render_json(["result"=>true]);

		}else{

			$this->render_json(["result"=>false]);

		}
	}

	public function delete_payment(){

		$account_id = $this->input->get('account_id');
		$payment_id = $this->input->get('payment_id');

		if (!is_null($account_id) && !is_null($payment_id)) {

			$this->Payment_model->delete_payment($payment_id);

			redirect('cus_account/show_account_detail?account_id=' . $account_id ,'refresh');

		}

	}





}
