<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Account Controller
 */



class Report extends SheepCode_Controller {

	function __construct()
  	{
	    parent::__construct();
        $this->load->library("PDF");
		set_secure_zone();
        $this->load->model('Account_model');
        $this->load->model('Payment_model');
        $this->load->model('Customer_model');
        $this->load->model('CustomerGroup_model');
  	}

  public function create_report()
  {

    $data['group'] = $this->CustomerGroup_model->find_all();
    $this->render('report/create_report',['title_page'=>"สร้างใบรายวันออก",'group'=>$data['group']]);

  }
   public function create_report_out()
  {

    $data['group'] = $this->CustomerGroup_model->find_all();
    $this->render('report/create_report_out',['title_page'=>"สร้างใบรายวันออก(พิเศษ)",'group'=>$data['group']]);

  }
  public function create_report_day()
  {

    $data['group'] = $this->CustomerGroup_model->find_all();
    $this->render('report/create_report_day',['title_page'=>"สร้างใบรายวันเข้า(ปกติ)",'group'=>$data['group']]);

  }
  public function create_report_day_s()
  {

    $data['group'] = $this->CustomerGroup_model->find_all();
    $this->render('report/create_report_day_s',['title_page'=>"สร้างใบรายวันเข้า(พิเศษ)",'group'=>$data['group']]);

  }
  public function create_report_month()
  {

    $data['group'] = $this->CustomerGroup_model->find_all();
    $this->render('report/create_report_month',['title_page'=>"สร้างใบรายเดือน(ปกติ)",'group'=>$data['group']]);

  }
  public function create_report_month_s()
  {

    $data['group'] = $this->CustomerGroup_model->find_all();
    $this->render('report/create_report_month_s',['title_page'=>"สร้างใบรายเดือน(พิเศษ)",'group'=>$data['group']]);

  }



  public function account_payment(){

    if ($this->input->get('account_id')== '' || $this->input->get('account_id') == null) {
        echo "error";
    }

    $account_id = $this->input->get('account_id');

    $payments = $this->Payment_model->find_by_accountid($account_id) ;

    $sum_payment = 0 ;

    foreach ($payments as $key => $value) {
        $sum_payment += $value->payment_amount;
    }


    $data = [
        "customer" => $this->Customer_model->find_by_accountid($account_id) ,
        "account" => $this->Account_model->find_by_id($account_id),
        "payments" => $payments,
        "payments_sum" =>$sum_payment
    ];

    // create new PDF document
    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    // remove default header/footer
    //$pdf->setHeaderData();
    //$pdf->setFooterData();
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(false);

    // set default monospaced font
    $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

    // set margins
    $pdf->SetMargins(10, 20.6, 15,10);
    $pdf->SetHeaderMargin(10);
    $pdf->SetFooterMargin(15);

    // set auto page breaks
    $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

    // set image scale factor
    $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

    // set some language-dependent strings (optional)
    if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
        require_once(dirname(__FILE__).'/lang/eng.php');
        $pdf->setLanguageArray($l);
    }

    $pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

    // set font
    $pdf->SetFont('angsanaupc', '', 15);
    $pdf->AddPage('P', 'A4');
    $pdf->setPage(true);

   // $html = "";

    $html = '<table border="0" width="300">';

    $html .= '<tr><td>ชื่อลูกค้า :</td><td>' . $data['customer']->customer_firstname . '   ' .$data['customer']->customer_lastname . '</td></tr>';
    $html .= '<tr><td>ชื่อบัญชี :</td><td>' . $data['account']->account_name . '</td></tr>';
    $html .= '<tr><td>ยอดกู้ :</td><td>' . $data['account']->account_outstanding . '</td></tr>';

    $outstand_inter = $data['account']->account_outstanding + ( $data['account']->account_outstanding * $data['account']->account_interest  );
    $html .= '<tr><td>ยอดกู้+ดอก :</td><td>' . $outstand_inter . '</td></tr>';
		$html .= '<tr><td>ส่งวันละ :</td><td>' . $data['account']->account_paypertimes . '</td></tr>';

    if ($data['account']->account_type != 2) {
        $balance  = $data['account']->account_outstanding + ( $data['account']->account_outstanding * $data['account']->account_interest  )  -   (($data['payments_sum']) ? $data['payments_sum'] : 0);
    }else{
        $balance = $data['account']->account_outstanding +  ( $data['account']->account_outstanding * $data['account']->account_interest  ) ;
    }


    $html .= '<tr><td>ยอดคงเหลือ :</td><td>' . $balance . '</td></tr>';
    $html .= '<tr><td>ยอดครวมส่ง :</td><td>' . $data['payments_sum'] . '</td></tr>';
    $html .= '</table>';

    $html .= '<br><br>';

    $html .= '<table border="1" >';
    $html .= '<tr>';
    $html .= '<th> วันที่เก็บ </th><th> ยอด</th>';
    $html .= '</tr>';

    foreach ($data['payments'] as $key => $value) {
        $html .= '<tr nobr="true"><td> ' ;
        $html .=  thai_month(formatDateToShow(date("Y-m-d",strtotime($value->payment_date)))). '</td>';
        $html .= '<td> '.$value->payment_amount  . '</td></tr>' ;

    }


    $html .= '</table>';




    //$html .=  '<b>ชื่อลูกค้า</b> :' . '<br> <b>นาสกุล :</b>' . $data['customer']->customer_lastname . '  ชื่อเล่น :' .$data['customer']->customer_nickname ;




    // Print text using writeHTMLCell()
    //$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, false, '', false);
    $pdf->writeHTML($html, true, false, false, false, '');
    // ---------------------------------------------------------

    // Close and output PDF document
    // This method has several options, check the source code documentation for more information.

    //$pdf->Output('example_001.pdf', 'I');
		$pdf->Output(date("d-m-Y").'.pdf', 'I');

    //============================================================+
    // END OF FILE
    //============================================================+
  }


}
