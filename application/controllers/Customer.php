<?php

/**
 * Example Controller
 */
class Customer extends SheepCode_Controller {

	function __construct()
  	{
	    parent::__construct();
			set_secure_zone();
	    $this->load->model('Customer_model');
      $this->load->model('CustomerGroup_model');
			$this->load->model('Account_model');
  	}
	public function create_form()
	{

    $data['group'] = $this->CustomerGroup_model->find_all();
		//$this->render('customer/create_form',$data);
		 $this->render('customer/create_form',['title_page'=>"Register Coustomer",'group'=>$data['group']]);

	}

 	public function read_form(){
		$group = $this->CustomerGroup_model->find_all();
    	$this->render('customer/read_form',['title_page'=>'จัดการ ข้อมูลลูกค้า','group'=>$group]);

  	}

	public function view_form(){
		$user = $this->Customer_model->find_by_id();
		if ($user != null) {
			$user->customer_pic = base64_encode($user->customer_pic);
		}

		$user_accounts = $this->Account_model->find_by_customer($this->input->get('customer_id'));

		$this->render('customer/view_detail',['title_page'=>'ข้อมูลลูกคต้า','customer'=>$user,'accounts'=>$user_accounts]);
	}


	public function create(){
   	 	$this->Customer_model->create();
		$this->session->set_flashdata("result_message","ทำการบันทึกข้อมูลสำเร็จ");
    	redirect('/cus/create_form','refresh');
  	}

// เรียกฟังชั่น delete จาก Customer_model

  	public function delete(){
    	$this->Customer_model->delete();
    	$this->render_json(['result'=>"success"]);
  	}



   public function datatable(){

		 if ($this->input->get('find_by_groupid')!= NULL && $this->input->get('find_by_groupid')!= '') {
			 $result_set = $this->Customer_model->find_by_groupid();
		 }else{
			 $result_set = $this->Customer_model->find_all();
		 }
    	$this->render_json(['data'=>$result_set]);
  	}

  	public function update(){
  		$this->Customer_model->update();
  		$this->render_json(['result'=>"success"]);
  	}

		public function update_img(){

			$update_result = $this->Customer_model->update_img();
			redirect('/cus/view?customer_id=' . $this->input->post('customer_id') ,'refresh');

		}






}
