<?php
/**
 * Author : ต่าย
 */
class Customer_model extends CI_Model
{

  public $customer_firstname ;
  public $customer_lastname ;
  public $customer_nickname ;
  public $customer_address ;
  public $customer_tel ;
  public $customer_pic ;
  public $customer_group_id ;
  //public $created_at ;

  private $table_name = "customers";

  function __construct()
  {
    parent::__construct();
  }

  public function find_by_groupid(){
    $this->db->select('customer_id as customer_id,,customer_id as cus_id,customer_firstname,customer_lastname,customer_nickname,customer_address,customer_tel');
    $this->db->where(['customer_group_id'=> $this->input->get('find_by_groupid')]);
    return $this->db->get($this->table_name)->result();
  }

  public function find_by_id(){

    $sql_command = "SELECT * FROM `customers` 
    INNER JOIN customer_groups ON customers.customer_group_id = customer_groups.customer_group_id 
    WHERE customers.customer_id = ?";
    $query = $this->db->query($sql_command,[$this->input->get('customer_id')]);
    return $query->row();
  }

  public function find_all(){

    $this->db->select('customer_id,customer_firstname,customer_lastname,customer_nickname,customer_address,customer_tel');
    return $this->db->get($this->table_name)->result();
  }

  public function find_by_accountid($account_id){

    $sql_command = "SELECT
                      *
                    FROM
                      customers
                    INNER JOIN
                      accounts ON accounts.customer_id = customers.customer_id
                    INNER JOIN
                      customer_groups ON customers.customer_group_id = customer_groups.customer_group_id
                    WHERE
                      accounts.account_id = ?";

    $query = $this->db->query($sql_command,[$account_id]);
    $q = $query->row();
    $q->customer_pic = base64_encode($q->customer_pic);

    return $q;

  }



  /**
   * create customer_groups method
   * สร้าง
   */
  public function create(){


    $config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'gif|jpg|png';

    $this->load->library('upload', $config);
    $this->upload->do_upload('customer_pic');
    $file_detail = $this->upload->data();

    if($file_detail['file_size'] <= 0){
      $this->customer_firstname = $this->input->post('customer_firstname');
      $this->customer_lastname = $this->input->post('customer_lastname');
      $this->customer_nickname = $this->input->post('customer_nickname');
      $this->customer_address = $this->input->post('customer_address');
      $this->customer_tel = $this->input->post('customer_tel');
      $this->customer_group_id = $this->input->post('customer_group_id');
      $this->db->insert($this->table_name,$this);


    }else{
      $this->customer_firstname = $this->input->post('customer_firstname');
      $this->customer_lastname = $this->input->post('customer_lastname');
      $this->customer_nickname = $this->input->post('customer_nickname');
      $this->customer_address = $this->input->post('customer_address');
      $this->customer_tel = $this->input->post('customer_tel');
      $this->customer_pic = (file_get_contents($file_detail['full_path']));
      $this->customer_group_id = $this->input->post('customer_group_id');
      $this->db->insert($this->table_name,$this);
      unlink($file_detail['full_path']);
    }

  }


  /**
   * update_img method
   */
  public function update_img(){

    $config['upload_path'] = './uploads/';
    $config['allowed_types'] = 'gif|jpg|png';

    $this->load->library('upload', $config);
    $this->upload->do_upload('customer_pic');
    $file_detail = $this->upload->data();

    //var_dump($file_detail);

    if ($file_detail['file_size'] <= 0) {
      return FALSE;
    }else{
      $this->db->update($this->table_name,['customer_pic'=> file_get_contents($file_detail['full_path']) ]
      ,["customer_id"=> $this->input->post('customer_id')]);
      unlink($file_detail['full_path']);
      return TRUE;
    }



  }


  /**
   * update customer_groups method
   * อัพเดรต
   * string append step by params get
   */
  public function update(){

    $sql_command = "UPDATE customers SET " ;

    if ($this->input->post('customer_firstname') != null) {
      $sql_command .= "customer_firstname = '" . $this->input->post('customer_firstname') . "'";
    }else if($this->input->post('customer_lastname') != null){
      $sql_command .= "customer_lastname = '" . $this->input->post('customer_lastname') . "'";
    }else if($this->input->post('customer_nickname') != null ){
      $sql_command .= "customer_nickname = '" . $this->input->post('customer_nickname') . "'";
    }else if($this->input->post('customer_tel') != null){
      $sql_command .= "customer_tel = '" . $this->input->post('customer_tel') . "'";
    }else if($this->input->post('customer_address') != null){
      $sql_command .= "customer_address = '" . $this->input->post('customer_address') . "'";
    }

    $sql_command .= " WHERE customer_id = '" . $this->input->post('customer_id') . "'";

    //var_dump($sql_command);
    $this->db->query($sql_command);
  }


  /**
   * delete customer_groups method
   * ลบ
   */
  public function delete(){

    $this->db->delete($this->table_name,['customer_id'=> $this->input->post('customer_id')]);

  }




}



 ?>
