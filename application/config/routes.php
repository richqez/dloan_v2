<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'Auth';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

$route['logout'] = 'Auth/logout';
$route['auth/login'] = 'Auth/login';
$route['auth'] = 'Auth/index';

$route['cus-group'] = 'CustomerGroup/read_form';
$route['cus-group/datatable'] = 'CustomerGroup/datatable';

$route['cus-group/create_form'] = 'CustomerGroup/create_form';
$route['cus-group/create'] = 'CustomerGroup/create';
$route['cus-group/update'] = 'CustomerGroup/update';
$route['cus-group/delete'] = 'CustomerGroup/delete';

$route['cus'] = 'Customer/read_form';
$route['cus/read_form'] = 'Customer/read_form';
$route['cus/datatable'] = 'Customer/datatable';
$route['cus/view'] = 'Customer/view_form';

$route['cus/create_form'] = 'Customer/create_form';
$route['cus/create'] = 'Customer/create';
$route['cus/update'] = 'Customer/update';
$route['cus/update_img'] = 'Customer/update_img';
$route['cus/delete'] = 'Customer/delete';

$route['cus_account/createaccount_form'] = 'Account/createaccount_form';
$route['cus_account/create_form'] = 'Account/create_form';
$route['cus_account/create'] = 'Account/create_account';
$route['cus_account/search'] = 'Account/search';
$route['cus_account/datatable'] = 'Account/datatable';
$route['cus_account/delete_account'] = 'Account/delete_account';
$route['cus_account/delete_payment'] = 'Account/delete_payment';
$route['cus_account/update_account_status'] = 'Account/update_account_status';

$route['cus_account/show_account_detail'] = 'Account/show_account_detail';
$route['cus_account/show_all_database'] = 'Account/show_account_detail_database';

$route['cus_account/paid'] = 'Account/paid';


$route['cus_report/create_report'] = 'Report/create_report';
$route['cus_report/create_report_out'] = 'Report/create_report_out';
$route['cus_report/create_report_day'] = 'Report/create_report_day';
$route['cus_report/create_report_month'] = 'Report/create_report_month';
$route['cus_report/create_report_day_s'] = 'Report/create_report_day_s';
$route['cus_report/create_report_month_s'] = 'Report/create_report_month_s';
