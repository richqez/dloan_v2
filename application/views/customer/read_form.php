<div class="banner">
  <h2>
      <a href="index.html">จัดการกลุ่มลูกค้า</a>
      <i class="fa fa-angle-right"></i>
      <span>ค้นหาและจัดการข้อมูลลูกค้า</span>
      </h2>
</div>



<div class="blank">
  <div class="blank-page">


  <div class="row">
    <div class="col-md-2" style="padding-right:0px">
      <nav class="nav-sidebar" style="margin:0px">
        <div class="content-box">
    			<ul id="customer_group_tab">
    			<li><span>กรองตามสายงาน</span></li>
          <li list-item class="active-bg"><a href="#" data-rowid="all"><i class="fa fa-group nav_icon" ></i> ทุกสายงาน </a></li>
          <?php foreach ($group as $key => $value): ?>
            <li list-item><a href="#" data-rowid="<?php echo $value->customer_group_id ?>"><i class="fa fa-group nav_icon" ></i> <?php echo $value->customer_group_name ?> </a></li>
          <?php endforeach; ?>
    			</ul>
        </div>

    	</nav>
    </div>
    <div class="col-md-10">
      <div class="blank-page">
        <table id="customer_table" class="table">
          <thead>
            <tr>
              <th>#</th>
              <th>ชื่อ</th>
              <th>นามสกุล</th>
              <th>ชื่อเล่น</th>
              <th>เบอร์</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
  </div>
</div>

<script type="text/javascript">

  var customer_table = $('#customer_table').DataTable({
    ajax       : '<?php echo base_url() . 'cus/datatable' ?>',
    pageLength : 100,
    columnDefs : [
      {
        "targets"    : [0] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [4] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [5] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [6] ,
        "orderable"  : false,
        "searchable" : false
      },
    ],
    columns    : [
      {data : null },
      {data : 'customer_firstname'},
      {data : 'customer_lastname'},
      {data : 'customer_nickname'},
      {data : 'customer_tel'},
      {
        mRender : function(data,type,full){
          return '<a href="#" data-action="edit" data-rowname="'+full.customer_firstname+'" data-rowid="'+full.customer_id+'" class="btn-primary btn"><span class="glyphicon glyphicon-pencil"></span></a>';
        },
        "sClass" : "center-text"
      },
      {
        mRender : function(data,type,full){
          return '<a href="#" data-action="delete" data-rowname="'+full.customer_firstname+'" data-rowid="'+full.customer_id+'" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>';
        },
        "sClass" : "center-text"
      }

    ],
    language: {
        "url" : '<?php echo base_url() . 'assert/th.json' ?>'
      }
  });

  customer_table.on( 'order.dt search.dt', function () {
    customer_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
      cell.innerHTML = i+1;
    } );
  } ).draw();

  $('#customer_table').on('click','a',function(){
    var click_btn = $(this);
    if (click_btn.data('action') == 'edit' ) {
      window.location="<?php echo base_url() ?>cus/view?customer_id="+click_btn.data('rowid');
    }else if (click_btn.data('action') == 'delete') {
      swal(
        {   title: "คุณต้องการที่จะลบ '"+click_btn.data('rowname')+"' ใช่ไหม?",
            text: "ถ้าคุณลบข้อมูลไม่แล้วจะไม่สามารถกู้คืนได้อีก...!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "ใช่, ลบเลย!",
            closeOnConfirm: false
        }, function(){
            var _rowid = click_btn.data('rowid');
            $.post('<?php echo base_url() . 'cus/delete'?>',{ customer_id: _rowid  })
              .done(function(data){
                swal("สำเร็จ!", "ข้อมูลถูกลบ.", "success");
                customer_table.ajax.reload();
              })
              .fail(function(data){
                sweetAlert("ขออภัย...", "มีบางอย่างผิดพลาด! ระบบเกิดปัญหา \n" + "statusCode :" +data.status + "statusText :" +data.statusText, "error");
              });
        });
    }

  });

  $('#customer_group_tab').on('click','a',function(){
    $('li[list-item]').removeClass('active-bg');
    $(this).parent('li').addClass('active-bg');
    var group_id = $(this).data('rowid');
    if (group_id === 'all') {
      customer_table.ajax.url('<?php echo base_url() . 'cus/datatable' ?>').load();
    }else{
      customer_table.ajax.url('<?php echo base_url() . 'cus/datatable' ?>'+'?find_by_groupid='+group_id).load();
    }
  })

</script>
