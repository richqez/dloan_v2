<div class="banner">
  <h2>
      <a href="index.html">จัดการกลุ่มลูกค้า</a>
      <i class="fa fa-angle-right"></i>
      <span>แสดงข้อมูลลูกค้า</span>
      </h2>
</div>

<div class="blank">
  <div class="blank-page">


    <?php if ($customer != null): ?>
      <div class="row">
        <div class="col-md-4 col-md-offset-4">
          <?php if (sizeof($customer->customer_pic) > 0): ?>
            <img src="data:image/png;base64,<?php echo  $customer->customer_pic ?>" alt="" class="img-thumbnail" />
            <?php else: ?>
              <img src="<?php echo img_url() ?>download.jpg" />
              <?php endif; ?>
        </div>
      </div>
      <br>
      <br>
      <div class="row">
        <div class="col-md-8 col-md-offset-2">
          <table class="table table-bordered" id="tbl_userview">
            <tr>
              <td>
                ชื่อ
              </td>
              <td>
                <?php echo $customer->customer_firstname; ?>
                <a href="#" data-edit="firstname" ><i class="glyphicon glyphicon-pencil" ></i></a>
              </td> 
              

            </tr>
            <tr>
              <td>
                นามสกุล
              </td>
              <td>
                <?php echo $customer->customer_lastname; ?>
                <a href="#" data-edit="lastname" ><i class="glyphicon glyphicon-pencil" ></i></a>
              </td>
            </tr>
            <tr>
              <td>
                ชื่อเล่น
              </td>
              <td>
                <?php echo $customer->customer_nickname; ?>
                <a href="#" data-edit="nickname" ><i class="glyphicon glyphicon-pencil"></i></a>
              </td>
            </tr>
            <tr>
              <td>
                ที่อยู่
              </td>
              <td>
                <?php echo $customer->customer_address; ?>
                <a href="#" data-edit="address" ><i class="glyphicon glyphicon-pencil"></i></a>
              </td>
            </tr>
            <tr>
              <td>
                เบอร์ติดต่อ
              </td>
              <td>
                <?php echo $customer->customer_tel ?>
                <a href="#" data-edit="tel" ><i class="glyphicon glyphicon-pencil"></i></a>
              </td>
            </tr>
          </table>
        </div>
      </div>

      <?php else: ?>
        <div class="row">
          <div class="col-md-6 col-md-offset-3" align="center">
            <h3>ไม่พบข้อมูลลูกค้า</h3>
          </div>
        </div>


        <?php endif; ?>
  </div>

</div>
<script type="text/javascript">

  var user_id = '<?php echo $customer->customer_id ?>';

  function editFirstName(){
    swal({   
      title: "หน้าต่างแก้ไขข้อมูล",  
      text: "แก้ไขชื่อจริง",   
      type: "input",   
      showCancelButton: true,  
      closeOnConfirm: false,   
      animation: "slide-from-top",   
      inputPlaceholder: "ชื่อใหม่" }, 
      function(inputValue)
      {   if (inputValue === false)
           return false;      
          if (inputValue === "") {    
           swal.showInputError("คุณจำเป็นต้องกรอกข้อมูล");     
           return false  
            }

            $.post('<?= base_url() ?>cus/update', {customer_id: user_id , customer_firstname : inputValue })
              .done(function(data){
                  console.log(data);
                  location.reload(true);
              })
              .fail(function(data){
                  console.log(data);
              });




          swal("ทำการบันทึกสำเร็จ", "ชื่อใหม่: " + inputValue, "success"); 
      });
  }

  function editLastName(){
    swal({   
      title: "หน้าต่างแก้ไขข้อมูล",  
      text: "แก้ไขนามสกุลจริง",   
      type: "input",   
      showCancelButton: true,  
      closeOnConfirm: false,   
      animation: "slide-from-top",   
      inputPlaceholder: "นามสกุลใหม่" }, 
      function(inputValue)
      {   if (inputValue === false)
           return false;      
          if (inputValue === "") {    
           swal.showInputError("คุณจำเป็นต้องกรอกข้อมูล");     
           return false  
            }

            $.post('<?= base_url() ?>cus/update', {customer_id: user_id , customer_lastname : inputValue })
              .done(function(data){
                  console.log(data);
                  location.reload(true);
              })
              .fail(function(data){
                  console.log(data);
              });




          swal("ทำการบันทึกสำเร็จ", "นามสกุลใหม่: " + inputValue, "success"); 
      });
  }

  function editNickName(){
    swal({   
      title: "หน้าต่างแก้ไขข้อมูล",  
      text: "แก้ไขชื่อเล่น",   
      type: "input",   
      showCancelButton: true,  
      closeOnConfirm: false,   
      animation: "slide-from-top",   
      inputPlaceholder: "ชื่อเล่นใหม่" }, 
      function(inputValue)
      {   if (inputValue === false)
           return false;      
          if (inputValue === "") {    
           swal.showInputError("คุณจำเป็นต้องกรอกข้อมูล");     
           return false  
            }

            $.post('<?= base_url() ?>cus/update', {customer_id: user_id , customer_nickname : inputValue })
              .done(function(data){
                  console.log(data);
                  location.reload(true);
              })
              .fail(function(data){
                  console.log(data);
              });




          swal("ทำการบันทึกสำเร็จ", "ชื่อเล่นใหม่: " + inputValue, "success"); 
      });
  }

  function editTel(){
    swal({   
      title: "หน้าต่างแก้ไขข้อมูล",  
      text: "แก้ไขเบอร์โทร",   
      type: "input",   
      showCancelButton: true,  
      closeOnConfirm: false,   
      animation: "slide-from-top",   
      inputPlaceholder: "แก้ไขเบอร์โทรใหม่" }, 
      function(inputValue)
      {   if (inputValue === false)
           return false;      
          if (inputValue === "") {    
           swal.showInputError("คุณจำเป็นต้องกรอกข้อมูล");     
           return false  
            }

            $.post('<?= base_url() ?>cus/update', {customer_id: user_id , customer_tel : inputValue })
              .done(function(data){
                  console.log(data);
                  location.reload(true);
              })
              .fail(function(data){
                  console.log(data);
              });

          swal("ทำการบันทึกสำเร็จ", "เบอร์โทรใหม่" + inputValue, "success"); 
      });
  }



  $('#tbl_userview').on('click', 'a', function(event) {
    event.preventDefault();
    var edit = $(this).data('edit');
    switch(edit){
      case 'firstname' : editFirstName(); break;
      case 'lastname' : editLastName(); break;
      case 'nickname' : editNickName(); break;
      case 'tel' : editTel(); break;
    }
  });


</script>
