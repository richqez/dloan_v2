<!--banner-->
<div class="banner">
  <h2>
      <a href="index.html">จัดการกลุ่มลูกค้า</a>
      <i class="fa fa-angle-right"></i>
      <span>สร้างบัญชี</span>
      </h2>
</div>

<div class="blank">
  <div class="blank-page">
    <div class="row">
      <div class="col-md-12 ">
        <form class="form-horizontal" id="create_account_frm">
          <div class="form-group">
            <label for="selector1" class="col-sm-3 control-label"><h4>ค้นหาลูกค้า</h4></label>
            <div class="col-sm-8">
              <select id="select2customer" style="width: 100%">

              </select>
            </div>
          </div>
        <div class="col-md-6">
            <div class="row">
              <label class="control-label">ข้อมูลลูกค้า</label>
              <br>
               <div class="col-md-8 offset-2"><hr></div>
            </div>
            <div class="row">
              <div class="form-group">
               <label for="selector1" class="col-sm-3 control-label">ชื่อ</label>
               <div class="col-sm-8">
                <input type="text" class="form-control"  name="customer_firstname" placeholder="ชื่อ" required="" title="คุณจำเป็นต้องกรอก" readonly="true">
              </div>
              </div>
           </div>
           <div class="row">
              <div class="form-group">
               <label for="selector1" class="col-sm-3 control-label">นามสกุล</label>
               <div class="col-sm-8">
                <input type="text" class="form-control"  name="customer_lastname" placeholder="นามสกุล" required="" title="คุณจำเป็นต้องกรอก" readonly="true">
              </div>
              </div>
           </div>
           <div class="row">
              <div class="form-group">
               <label for="selector1" class="col-sm-3 control-label">เบอร์โทรศัพท์</label>
               <div class="col-sm-8">
                <input type="text" class="form-control"  name="customer_tel" placeholder="เบอร์โทรศัพท์" required="" title="คุณจำเป็นต้องกรอก" readonly="true">
              </div>
              </div>
           </div>
        </div>
        <div class="col-md-6">
            <div class="row">
              <label class="control-label">ข้อมูลการยืม</label>
              <br>
              <div class="col-md-8 offset-2"><hr></div>
            </div>
            <div class="row">
              <div class="form-group">
               <label for="selector1" class="col-sm-3 control-label">ยอดกู้</label>
               <div class="col-sm-8">
                <input type="text" class="form-control"  name="account_outstanding" placeholder="ยอดกู้" required="" title="คุณจำเป็นต้องกรอก" autocal >
              </div>
              </div>
           </div>
          <div class="row">
              <div class="form-group">
               <label for="selector1" class="col-sm-3 control-label">วันที่กู้ยืม</label>
               <div class="col-sm-8">
                <input type="text" class="form-control" id="account_startdate" name="account_startdate" placeholder="วันที่กู้ยืม" required="" title="คุณจำเป็นต้องกรอก" >
              </div>
              </div>
           </div>
           <div class="row">
              <div class="form-group">
               <label for="selector1" class="col-sm-3 control-label">ดอกเบี้ย</label>
               <div class="col-sm-8">
                <input type="text" class="form-control" name="account_interest" placeholder="ดอกเบี้ย" value="20" maxlength="2" required="" title="คุณจำเป็นต้องกรอก" autocal >
              </div>
              </div>
           </div>
           <div class="row">
              <div class="form-group">
               <label for="selector1" class="col-sm-3 control-label">ประเภทกู้</label>
               <div class="col-sm-8">
               <select class="form-control" name="account_type" autocal >
                  <option value="0">รายเดือน</option>
                  <option value="1">รายวัน</option>
                  <option value="2">ดอกลอย</option>
               </select>
              </div>
              </div>
           </div>
           <div class="row">
              <div class="form-group">
               <label for="selector1" class="col-sm-3 control-label">ระยะเวลา</label>
               <div class="col-sm-8">
                <input type="text" class="form-control" name="account_duration" placeholder="ระยะเวลา" required="" title="คุณจำเป็นต้องกรอก" autocal >
              </div>
              </div>
           </div>
           <div class="row">
              <div class="form-group">
               <label for="selector1" class="col-sm-3 control-label">ยอดกู้+ดอก</label>
               <div class="col-sm-8">
                <input type="text" class="form-control" id="oandi"  required="" title="คุณจำเป็นต้องกรอก"  readonly="">
              </div>
              </div>
           </div>
           <div class="row">
              <div class="form-group">
               <label for="selector1" class="col-sm-3 control-label">ยอดชำระต่อครั้ง/วัน</label>
               <div class="col-sm-8">
                <input type="text" class="form-control" name="account_payperday"  required="" title="คุณจำเป็นต้องกรอก" >
              </div>
              </div>
           </div>
             <div class="row">
              <div class="form-group">
               <label  class="col-sm-3 control-label">สิ่งที่นำมารับประกัน</label>
               <div class="col-sm-8">
                <textarea class="form-control" name="account_guarantee" rows="5" id="comment"></textarea>
              </div>
              </div>
           </div>
           <div class="row">
              <div class="form-group">
                <div class="col-sm-3 control-label">

                </div>
                <div class="col-sm-8 ">
                  <input type="hidden" name="customer_id" value="-1">
                  <input type="submit" class="btn btn-primary" value="ยืนยัน">
                  <input type="reset" class="btn btn-danger" value="ยกเลิก">
                </div>
              </div>
           </div>

        </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">

$('#auto_cal-btn').click(function(){





})

function auto_cal(){
  var result = 0 ;
  var account_type  = $('select[name=account_type]').val();
  var duration = $('input[name=account_duration]').val();
  var interest = $('input[name=account_interest]').val();
  var outstanding = $('input[name=account_outstanding]').val();

  var  r = (parseInt(outstanding) * parseInt(interest)) /100;
  result  = parseInt(outstanding)  + parseInt(r)  ;
  console.log(account_type);
  if (account_type == 2) {
    if (!isNaN(r)) {
      $('input[name=account_payperday]').val(r);
      $('#oandi').val(outstanding);
      $('input[name=account_interest]').val(0);
    }
  }else{
    if (isNaN(result)) {
      $('#oandi').val(0);
    }else{
      $('#oandi').val(result);
      var pd = parseInt(result) / parseInt(duration);
      if (!isNaN(pd)) {
        $('input[name=account_payperday]').val(pd);
      }
    }
  }



}

$(document).on('change', 'input[autocal],select[autocal]', function(event) {
  event.preventDefault();
  auto_cal();
});


$('#select2customer').select2({
    ajax: {
      url: "<?php echo base_url() . 'cus_account/search' ?>",
      dataType: 'json',
      processResults: function (data) {
        return {
          results: $.map(data,function(item){
            return{
              text: item.name,
              id: item.id,
              full: item.fulldata
            }
          })
        };
      }
  }
});

$("#select2customer").on('select2:select',function(event) {
  event.preventDefault();
  var user_select_data = event.params.data.full;
  //console.log(user_select_data);

  $.get( "<?php echo base_url() . 'Account/isValidCustomer?user_id=' ?>"+user_select_data.customer_id  ,function(result){
    if (result) {
      $("input[name=customer_firstname]").val(user_select_data.customer_firstname);
      $("input[name=customer_lastname]").val(user_select_data.customer_lastname);
      $("input[name=customer_tel]").val(user_select_data.customer_tel);
      $("input[name=customer_id]").val(user_select_data.customer_id);
    }else{
      alert("ไม่สามารถ สร้างบัญชีได้เนื่องจาก ลูกค้ายังไม่ได้ปิดบัญชี");
      $("input[name=customer_firstname]").val("");
      $("input[name=customer_lastname]").val("");
      $("input[name=customer_tel]").val("");
      $("input[name=customer_id]").val("");
    }
  })




});

var startDate =  $('#account_startdate').datepicker({
    language:'th',
    format:'dd-mm-yyyy',
    todayHighlight :true,
    todayBtn:true,
    orientation:'bottom',
    autoclose:true
    });


 $('#create_account_frm').submit(function(event){
   event.preventDefault();

   var account_data = $(this).serializeObject();

   if($("input[name=customer_id]").val() != -1 || $("input[name=customer_id]").val() != '-1' ){
     swal({
       title: "คุณต้องการจะสร้างบัญชีใช่หรือไม่?",
       text: "คุณต้องการจะสร้างบัญชีให้กับลูกค้าชื่อ" + $("input[name=customer_firstname]").val() + ' ?' ,
       type: "warning",
       showCancelButton: true,
       confirmButtonColor: "#DD6B55",
       confirmButtonText: "ใช่ฉันต้องการ, สร้างเลย",
       cancelButtonText: "ไม่เป็นไร, คราวหลัง",
       closeOnConfirm: false,
       closeOnCancel: false },
       function(isConfirm){
          if (isConfirm) {

              $.post('<?= base_url() ?>cus_account/create',account_data)
               .done(function(){
                 swal("ทำรายการสำเร็จ", "ระบบทำการสร้างบัญชีเรียบร้อยแล้ว", "success");
                 location.reload(true);
               })
               .fail(function(){
                  swal("Cancelled", "โปรด ตรวจสอบข้อมูล และ ทำรายการอีกครั้ง :)", "error");
               });
          }
          else {
           }
         });
   }else{
     swal("ไม่สามรถทำรายการได้", "กรุณาเลือกลูกค้าก่อนทำรายการ", "error");
   }

 });

    $('input[name=account_outstanding]').focusout(function(event) {
      if (isNaN($(this).val())) {
        swal("ผู้ใช้ใส่ข้อมูลผิดพลาด", "  'ยอดกู้ ' จำเป็นต้องใส่เป็นตัวเลขเท่านั้น!", "error");
        $(this).val('');
        $(this).focus();
      }
    });












</script>
