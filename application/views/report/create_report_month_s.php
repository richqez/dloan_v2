<!--banner-->
<div class="banner">
  <h2>
      <a href="index.html">จัดการรายงาน</a>
      <i class="fa fa-angle-right"></i>
      <span>สร้างใบรายเดือน(พิเศษ)</span>
      </h2>
</div>

<div class="blank">
  <div class="blank-page">
  <div class="grid-form">
    <div class="grid-form1">
      <h4 id="forms-example" class="">สร้างใบรายเดือน(พิเศษ)</h4>
      <form id="create_form" target="_blank" class="form-horizontal" action="<?php echo base_url() . 'report/create_report_payment_month_s.php' ?>" method="get">
         <div class="form-group">
               <label for="selector1" class="col-sm-2 control-label">วันที่</label>
               <div class="col-sm-4">
                <input type="text" class="form-control" id="account_startdate" name="account_startdate" placeholder="วันที่" required="" title="คุณจำเป็นต้องกรอก" >
              </div>
          </div>
         <div class="form-group">
          <label for="inputPassword3" class="col-sm-2 control-label hor-form">สายงาน</label>
          <div class="col-sm-4">
            <select class="form-control" name="customer_group_id" id="customer_group_id">
                 
                  <?php foreach ($group as  $value): ?>
                    <option value="<?php echo $value->customer_group_id;?> "><?php echo $value->customer_group_name; ?></option>
                  <?php endforeach; ?>
            </select>
          </div>
        </div>
      
        <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
              <button class="btn-primary btn" id="sub" type="submit">ตกลง</button>
        
          </div>
        </div>
      </form>
  </div>
  </div>
  <script type="text/javascript">

  var startDate =  $('#account_startdate').datepicker({
      language:'th',
      format:'dd-mm-yyyy',
      todayHighlight :true,
      todayBtn:true,
      orientation:'bottom',
      autoclose:true
      });

  </script>
  