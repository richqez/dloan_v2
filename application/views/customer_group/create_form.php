<!--banner-->
<div class="banner">
  <h2>
      <a href="index.html">จัดการสายงาน</a>
      <i class="fa fa-angle-right"></i>
      <span>สร้างสายงาน</span>
      </h2>
</div>
<div class="grid-form">
  <div class="grid-form1">
    <?php if ($this->session->flashdata("result_message")!==NULL): ?>
        <div class="alert alert-success">
            <strong> <?php echo $this->session->flashdata("result_message") ?> !</strong>
        </div>
    <?php endif; ?>
    <?php if ($this->session->flashdata("result_message-error")!==NULL): ?>
      <div class="alert alert-danger" role="alert">
        <strong> <?php echo $this->session->flashdata("result_message-error") ?> !</strong>
      </div>
    <?php endif; ?>
    <h3 id="forms-example" class="">สร้างสายงาน</h3>
    <form action="<?php echo base_url() . 'cus-group/create' ?>" method="post" class ="form-horizontal" id="customer_group_frm">
      <!-- <div class="form-group">
        <label>ชื่อกลุ่ม</label>
        <input type="text" class="form-control" placeholder="ชื่อกลุ่มลูกค้า" name="customer_group_name">
      </div> -->
      <div class="form-group">
          <label for="inputEmail3" class="col-sm-2 control-label hor-form">ชื่อสายงาน</label>
          <div class="col-sm-8">
            <input type="text" class="form-control" id="cus-group-name" name="customer_group_name" placeholder="ชื่อสายงาน">
          </div>
      </div>
      <div class="form-group">
          <div class="col-sm-offset-2 col-sm-10">
              <button class="btn-primary btn" type="submit">สร้าง</button>
              <button class="btn-default btn">ยกเลิก</button>

          </div>
      </div>
    </form>
  </div>
</div>

<script type="text/javascript">
  $('#customer_group_frm').submit(function(event) {
    if($('#cus-group-name').val() === '' || $('#cus-group-name') === null){
      swal("ไม่สามารถทำรายการได้", "คุณจำเป็นต้องกรอกข้อมูลให้ครบก่อน: ", 'error');
      event.preventDefault();
    }
  });
</script>
