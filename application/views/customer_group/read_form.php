<div class="banner">
  <h2>
      <a href="index.html">จัดการสายงาน</a>
      <i class="fa fa-angle-right"></i>
      <span>ค้นหาและจัดการข้อมูลสายงาน</span>
      </h2>
</div>


<div class="blank">
  <div class="blank-page">
    <div class="row">
      <div class="col-md-12">
        <table id="customer_group_table" class="table">
          <thead>
            <tr>
              <th>#</th>
              <th>ชื่อสายงาน</th>
              <th>สร้างเมื่อวันที่</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

  var customer_group_table = $('#customer_group_table').DataTable({
    ajax       : '<?php echo base_url() . 'cus-group/datatable' ?>',
    pageLength : 100,
    columnDefs : [
      {
        "targets"    : [0] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [3] ,
        "orderable"  : false,
        "searchable" : false
      },
      {
        "targets"    : [4] ,
        "orderable"  : false,
        "searchable" : false
      },
    ],
    columns    : [
      {data : null },
      {data : 'customer_group_name'},
      {data : 'created_at'},
      {
        mRender : function(data,type,full){
          return '<a href="#" data-action="edit" data-rowname="'+full.customer_group_name+'" data-rowid="'+full.customer_group_id+'" class="btn-primary btn"><span class="glyphicon glyphicon-pencil"></span></a>';
        },
        "sClass" : "center-text"
      },
      {
        mRender : function(data,type,full){
          return '<a href="#" data-action="delete" data-rowname="'+full.customer_group_name+'" data-rowid="'+full.customer_group_id+'" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></a>';
        },
        "sClass" : "center-text"
      }
    ],
    language: {
        "url" : '<?php echo base_url() . 'assert/th.json' ?>'
      }
  });

  customer_group_table.on( 'order.dt search.dt', function () {
    customer_group_table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
      cell.innerHTML = i+1;
    } );
  } ).draw();

  $('#customer_group_table').on('click','a',function(){
    var click_btn = $(this);
    if (click_btn.data('action') == 'edit' ) {
       swal(
           {
             title              : "แก้ไขชื่อสายงาน",
              text               : "เปลี่ยนชื่อสายงาน '" + click_btn.data('rowname') + "'",
              type               : "input",
              showCancelButton   : true,
              closeOnConfirm     : false,
              confirmButtonText  : "ใช่, ทำการเปลี่ยนแปลงเลย!",
              animation          : "slide-from-top",
              inputPlaceholder   : "ชื่อที่ต้องการเปลี่ยน",
              showLoaderOnConfirm: true

           },function(inputValue){
              if (inputValue === false) return false;
              if (inputValue === "") {
                 swal.showInputError("คุณต้องพิมอะไรบางอยาง....");
                 return false
               }
                 var _rowid = click_btn.data('rowid');
                 $.post('<?php echo base_url() . 'cus-group/update' ?>',{ customer_group_id : _rowid , customer_group_name : inputValue  })
                  .done(function(data){
                    if (data.result) {
                      swal("สำเร็จ!", "คุณทำการอัพเดรตข้อมูล " + inputValue, "success");
                    }else{
                      swal("ไม่สำเร็จ!", "ชื่อ " + click_btn.data('rowname') + " มีอยู่แล้ว โปรดลองใหม่" , "error");
                    }
                    customer_group_table.ajax.reload();
                  })
                  .fail(function(data){
                    sweetAlert("ขออภัย...", "มีบางอย่างผิดพลาด! ระบบเกิดปัญหา \n" + "statusCode :" +data.status + "statusText :" +data.statusText, "error");
                  });
                });

    }else if (click_btn.data('action') == 'delete') {
      swal(
        {
            title              : "คุณต้องการที่จะลบ '"+click_btn.data('rowname')+"' ใช่ไหม?",
            text               : "ถ้าคุณลบข้อมูลไม่แล้วจะไม่สามารถกู้คืนได้อีก...!",
            type               : "warning",
            showCancelButton   : true,
            confirmButtonColor : "#DD6B55",
            confirmButtonText  : "ใช่, ลบเลย!",
            closeOnConfirm     : false,
            showLoaderOnConfirm: true

        }, function(){
            var _rowid = click_btn.data('rowid');
            $.post('<?php echo base_url() . 'cus-group/delete'?>',{ rowid: _rowid  })
              .done(function(data){
                swal("สำเร็จ!", "ข้อมูลถูกลบ.", "success");
                customer_group_table.ajax.reload();
              })
              .fail(function(data){
                sweetAlert("ขออภัย...", "มีบางอย่างผิดพลาด! ระบบเกิดปัญหา \n" + "statusCode :" +data.status + "statusText :" +data.statusText, "error");
              });

        });
    }

  });

</script>
