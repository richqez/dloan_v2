<nav class="navbar-default navbar-static-top" role="navigation">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <h1> <a class="navbar-brand" href="<?php echo base_url() . 'cus-group' ?>">D Loan</a></h1>
  </div>
  <div class=" border-bottom">
    <div class="full-left">
      <section class="full-top">
        <button id="toggle"><i class="fa fa-arrows-alt"></i></button>
      </section>
      <div class="clearfix"> </div>
    </div>
    <!-- Brand and toggle get grouped for better mobile display -->

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="drop-men">
      <ul class=" nav_1">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle dropdown-at" data-toggle="dropdown"><span class=" name-caret">Rackham<i class="caret"></i></span><img width="60px" height="60px" src="<?php echo img_url() ?>avatar.jpg"></a>
          <ul class="dropdown-menu " role="menu">
            <li><a href="profile.html"><i class="fa fa-user"></i>Edit Profile</a></li>
            <li><a href="inbox.html"><i class="fa fa-envelope"></i>Inbox</a></li>
            <li><a href="calendar.html"><i class="fa fa-calendar"></i>Calender</a></li>
            <li><a href="inbox.html"><i class="fa fa-clipboard"></i>Tasks</a></li>
          </ul>
        </li>

      </ul>
    </div>
    <!-- /.navbar-collapse -->
    <div class="clearfix">

    </div>

    <div class="navbar-default sidebar" role="navigation">
      <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">

         <!--  <li>
            <a href="index.html" class=" hvr-bounce-to-right"><i class="fa fa-dashboard nav_icon "></i><span class="nav-label">Dashboards</span> </a>
          </li>

          <li>
            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-indent nav_icon"></i> <span class="nav-label">Menu Levels</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="graphs.html" class=" hvr-bounce-to-right"> <i class="fa fa-area-chart nav_icon"></i>Graphs</a>
              </li>

              <li><a href="maps.html" class=" hvr-bounce-to-right"><i class="fa fa-map-marker nav_icon"></i>Maps</a></li>

              <li><a href="typography.html" class=" hvr-bounce-to-right"><i class="fa fa-file-text-o nav_icon"></i>Typography</a></li>

            </ul>
          </li>
          <li>
            <a href="inbox.html" class=" hvr-bounce-to-right"><i class="fa fa-inbox nav_icon"></i> <span class="nav-label">Inbox</span> </a>
          </li>

          <li>
            <a href="gallery.html" class=" hvr-bounce-to-right"><i class="fa fa-picture-o nav_icon"></i> <span class="nav-label">Gallery</span> </a>
          </li>
          <li>
            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-desktop nav_icon"></i> <span class="nav-label">Pages</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li>
                <a href="404.html" class=" hvr-bounce-to-right"> <i class="fa fa-info-circle nav_icon"></i>Error 404</a>
              </li>
              <li><a href="faq.html" class=" hvr-bounce-to-right"><i class="fa fa-question-circle nav_icon"></i>FAQ</a></li>
              <li><a href="blank.html" class=" hvr-bounce-to-right"><i class="fa fa-file-o nav_icon"></i>Blank</a></li>
            </ul>
          </li>
          <li>
            <a href="layout.html" class=" hvr-bounce-to-right"><i class="fa fa-th nav_icon"></i> <span class="nav-label">Grid Layouts</span> </a>
          </li>

          <li>
            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-list nav_icon"></i> <span class="nav-label">Forms</span><span class="fa arrow"></span></a>
            <ul class="nav nav-second-level">
              <li><a href="forms.html" class=" hvr-bounce-to-right"><i class="fa fa-align-left nav_icon"></i>Basic forms</a></li>
              <li><a href="validation.html" class=" hvr-bounce-to-right"><i class="fa fa-check-square-o nav_icon"></i>Validation</a></li>
            </ul>
          </li>
 -->
          <!-- <li>
            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-group nav_icon"></i> <span class="nav-label">จัดการสายงาน</span><span class="fa arrow"></span></a> -->
            <ul class="nav nav-second-level" style="background-color: rgba(161, 98, 193, 0.23);">
              <li><a href="<?php echo base_url() . 'cus-group' ?>" class=" hvr-bounce-to-right"><i class="fa fa-users nav_icon"></i>ค้นหาและจัดการสายงาน</a></li>
              <li><a href="<?php echo base_url() . 'cus-group/create_form' ?>"><i class="fa fa-users nav_icon"></i>สร้างสายงาน</a></li>
            </ul>
          <!-- </li> -->
          <!-- <li>
            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-user nav_icon"></i> <span class="nav-label">จัดการลูกค้า</span><span class="fa arrow"></span></a> -->
            <ul class="nav nav-second-level" style=" background-color: rgba(255, 0, 0, 0.23);">
              <li><a href="<?php echo base_url() . 'cus/read_form' ?>" class=" hvr-bounce-to-right"><i class="fa fa-male nav_icon"></i>ค้นหาและจัดการลูกค้า</a></li>
              <li><a href="<?php echo base_url() . 'cus/create_form' ?>"><i class="fa fa-male nav_icon"></i>สร้างข้อมูลลูกค้า</a></li>
            </ul>
          <!-- </li> -->
          <!-- <li>
            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-book nav_icon"></i> <span class="nav-label">จัดการบัญชีลูกค้า</span><span class="fa arrow"></span></a> -->
            <ul class="nav nav-second-level" style="background-color: rgba(255, 255, 0, 0.23);">
              <li><a href="<?php echo base_url() . 'cus_account/create_form' ?>" class=" hvr-bounce-to-right"><i class="fa fa-money nav_icon"></i>ค้นหาและจัดการบัญชี</a></li>
              <li><a href="<?php echo base_url() . 'cus_account/createaccount_form' ?>" class=" hvr-bounce-to-right"><i class="fa fa-money nav_icon"></i>สร้างบัญชี</a></li>
              <li>
            </ul>
          <!-- </li> -->
          <!-- <li>
            <a href="#" class=" hvr-bounce-to-right"><i class="fa fa-print nav_icon"></i> <span class="nav-label">จัดการรายงาน</span><span class="fa arrow"></span></a> -->
            <ul class="nav nav-second-level" style="    background: rgba(26, 188, 156, 0.23);">
              <li><a href="<?php echo base_url() . 'cus_report/create_report' ?>" class=" hvr-bounce-to-right"><i class="fa fa-book nav_icon"></i>ใบรายวันออก(ปกตื)</a></li>

              <li><a href="<?php echo base_url() . 'cus_report/create_report_out' ?>" class=" hvr-bounce-to-right"><i class="fa fa-book nav_icon"></i>ใบรายวันออก(พิเศษ)</a></li>
              <li><a href="<?php echo base_url() . 'cus_report/create_report_day' ?>" class=" hvr-bounce-to-right"><i class="fa fa-book nav_icon"></i>ใบรายวันเข้า(ปกติ)</a></li>
              <li><a href="<?php echo base_url() . 'cus_report/create_report_day_s' ?>" class=" hvr-bounce-to-right"><i class="fa fa-book nav_icon"></i>ใบรายวันเข้า(พิเศษ)</a></li>

              <li><a href="<?php echo base_url() . 'cus_report/create_report_month' ?>" class=" hvr-bounce-to-right"><i class="fa fa-book nav_icon"></i>ใบรายเดือนเข้า(ปกติ)</a></li>
              <li><a href="<?php echo base_url() . 'cus_report/create_report_month_s' ?>" class=" hvr-bounce-to-right"><i class="fa fa-book nav_icon"></i>ใบรายเดือนเข้า(พิเศษ)</a></li>
            </ul>
         <!--  </li>  -->
        </ul>
      </div>
    </div>
</nav>
