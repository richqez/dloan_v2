<?php

//สายงาน
$group_id = $_GET['customer_group_id'];

$con=mysqli_connect("localhost","root","","d_loan");
$con->set_charset("utf8");
$dbcon = mysqli_query($con,"SELECT * FROM customer_groups
                            where customer_group_id = $group_id
                      ");
//สายงาน
// SELECT * FROM accounts INNER JOIN customers ON accounts.customer_id = customers.customer_id WHERE customers.customer_group_id = 1
$row = mysqli_fetch_array($dbcon);
$group_name = $row['customer_group_name'];

require_once('../tcpdf/tcpdf.php');
class MYPDF extends TCPDF {

    //Page header
    public function Header() {
        $group_id = $_GET['customer_group_id'];

        $con=mysqli_connect("localhost","root","","d_loan");
        $con->set_charset("utf8");
        $dbcon = mysqli_query($con,"SELECT * FROM customer_groups
                                    where customer_group_id = $group_id
                              ");
        $row = mysqli_fetch_array($dbcon);
        $group_name = $row['customer_group_name'];
        $this->SetFont('angsanaupc', 'B', 16);
        $date =  $_GET['account_startdate'];
          list($d,$m,$Y) = split('-',$date);
          switch($m) {
          case "01":  $m = "ม.ค."; break;
          case "02":  $m = "ก.พ."; break;
          case "03":  $m = "มี.ค."; break;
          case "04":  $m = "เม.ย."; break;
          case "05":  $m = "พ.ค."; break;
          case "06":  $m = "มิ.ย."; break;
          case "07":  $m = "ก.ค."; break;
          case "08":  $m = "ส.ค."; break;
          case "09":  $m = "ก.ย."; break;
          case "10":  $m = "ต.ค."; break;
          case "11":  $m = "พ.ย."; break;
          case "12":  $m = "ธ.ค."; break;
          }
        $this->Cell(0, 0, 'วันที่ออกเอกสาร '.$d.'/'.$m.'/'.$Y.'   สายงานเก็บ '.$group_name.'', 0, 0, 'L');


    }

     public function Footer() {
        $this->SetFont('angsanaupc', 'B', 16);

        $this->Cell(210, 0, 'หน้า '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, 0, 'R');
     }

}

// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// remove default header/footer
$pdf->setHeaderData();
$pdf->setFooterData();
$pdf->setPrintHeader(true);
$pdf->setPrintFooter(true);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(10, 20.6, 15,10);
$pdf->SetHeaderMargin(10);
$pdf->SetFooterMargin(15);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

$pdf->SetDisplayMode('fullpage', 'SinglePage', 'UseNone');

// set font
$pdf->SetFont('angsanaupc', '', 15);
$pdf->AddPage('P', 'A4');
$pdf->setPage(true);

// Date
$date =  $_GET['account_startdate'];
list($d,$m,$Y) = split('-',$date);
switch($m) {
case "01":  $m = "ม.ค."; break;
case "02":  $m = "ก.พ."; break;
case "03":  $m = "มี.ค."; break;
case "04":  $m = "เม.ย."; break;
case "05":  $m = "พ.ค."; break;
case "06":  $m = "มิ.ย."; break;
case "07":  $m = "ก.ค."; break;
case "08":  $m = "ส.ค."; break;
case "09":  $m = "ก.ย."; break;
case "10":  $m = "ต.ค."; break;
case "11":  $m = "พ.ย."; break;
case "12":  $m = "ธ.ค."; break;
}

$theader = '<table border="1">';
$theader .= '<theader>
           <tr>
             <th align="center" width="90px">ชื่อบัญชี</th>
             <th align="center">จำนวนเงินต้น</th>
             <th align="center">ยอดคงเหลือ</th>
             <th align="center">วันที่กู้</th>
             <th align="center">ส่งคืนวันล่ะ</th>
             <th align="center">ประเภท</th>
             <th align="center">ชื่อเล่น</th>
             <th align="center" colspan="2">ที่อยู่</th>
           </tr>
           </theader>';

$con->set_charset("utf8");
$dbcon = mysqli_query($con,"SELECT account_id,account_duration ,account_paypertimes ,customer_address ,account_startdate,account_status,account_name,customer_tel,customer_nickname ,customer_firstname,customer_group_name,IFNULL(SUM(sum_payment_amount),0) as sum_payment_amount,SUM(account_interest*100) AS account_interest ,account_outstanding ,CASE account_type WHEN 0 THEN 'รายเดือน'
      WHEN 1 THEN 'รายวัน' ELSE 'ดอกลอย'
      END AS account_type  ,
      IF(account_type = 2 ,account_outstanding + (account_outstanding*account_interest),COALESCE( (account_outstanding + (account_outstanding*account_interest)) - SUM(sum_payment_amount),(account_outstanding + (account_outstanding*account_interest))) ) as balances,
      account_outstanding + (account_outstanding*account_interest) as st

      FROM(
          SELECT customers.customer_group_id as customer_group_id,customer_nickname as customer_nickname ,accounts.account_id as account_id,account_paypertimes  as account_paypertimes ,account_status as account_status,customer_address as customer_address,account_duration as account_duration  ,account_startdate as account_startdate ,account_name as account_name,account_type,customer_tel,customer_group_name,customer_firstname,payment_amount as sum_payment_amount,account_interest,account_outstanding
          FROM accounts
          LEFT JOIN payments ON payments.account_id = accounts.account_id
          INNER JOIN customers ON customers.customer_id = accounts.customer_id
          INNER JOIN customer_groups ON customer_groups.customer_group_id = customers.customer_group_id

      ) as payment_all WHERE account_status = 3 AND customer_group_id = $group_id  GROUP BY (account_id) ORDER BY account_name ASC ");
$j = 1;
while($row = mysqli_fetch_array($dbcon))
{

   $cDatestart = explode("-", $row['account_startdate']);
    switch ($cDatestart[1]) {
              case '01':$cDatestart[1]  = "ม.ค." ;break;
              case '02':$cDatestart[1]  = "ก.พ.";break;
              case '03':$cDatestart[1]  = "มี.ค.";break;
              case '04':$cDatestart[1]  = "เม.ย.";break;;
              case '05':$cDatestart[1]  = "พ.ค.";break;
              case '06':$cDatestart[1]  = "มิ.ย.";break;
              case '07':$cDatestart[1]  = "ก.ค.";break;
              case '08':$cDatestart[1]  = "ส.ค.";break;
              case '09':$cDatestart[1]  = "ก.ย.";break;
              case '10':$cDatestart[1]  = "ต.ค.";break;
              case '11':$cDatestart[1]  = "พ.ย.";break;
              case '12':$cDatestart[1]  = "ธ.ค.";break;
              default:
                # code...
                break;
            }
   $account_name_new = explode("#", $row['account_name']);

   $cDatestart[0] = $cDatestart[0]+543;
   $st = $row['st'];

  $sum_payment_amount = $row['sum_payment_amount'];

   $sum_total = $row['account_outstanding']+($row['account_outstanding']* ($row['account_interest']/100));
   $stay = $row['balances'];

   $balances = $row['balances'];
   if($row['account_status'] == '2'){
    $theader .= '<tr style="color:red">';
   }elseif($balances == 0 && $row['account_type'] != 'รายเดือน'){
    $theader .= '<tr style="color:green">';
   }else{
    $theader .= '<tr>';
   }

  if($row['account_status'] == '2'){
        $theader .= '
             <td style="font-size:15px"><u> '.$account_name_new[0].'</u> </td>
             ';
        if ($row['account_type'] == 'รายวัน') {


          $theader .= '<td align="right" ><u>'.$row['account_outstanding'].' บ.</u></td>';
          $theader .= '<td align="right" ><u>'.$balances.' บ.</u></td>';

          $theader .= '
                 <td align="center" ><u>'.$cDatestart[2].'/'.$cDatestart[1].'/'.substr($cDatestart[0],2,4).'</u></td>';
          $theader .= '<td align="right" ><u>'.$row['account_paypertimes'].' บ.</u></td>';
          $theader .= '<td><u> '.$row['account_duration'].' วัน</u> </td>';

        }elseif ($row['account_type'] == 'ดอกลอย') {


          $theader .= '<td align="right" ><u> '.$row['account_outstanding'].' บ.</u></td>';
          $theader .= '<td align="right" ><u> '.$row['account_outstanding'].' บ.</u></td>';

          $theader .= '
                 <td align="center" ><u> '.$cDatestart[2].'/'.$cDatestart[1].'/'.substr($cDatestart[0],2,4).'</u></td>';
          $theader .= '<td align="right" ><u> '.$row['account_paypertimes'].' บ.</u></td>';
          $theader .= '<td><u> ดอกลอย<u/> </td>';

        }elseif ($row['account_type'] == 'รายเดือน'){


          $theader .= '<td align="right" ><u> '.$row['account_outstanding'].' บ.</u></td>';
          if($row['account_type'] == 'รายเดือน'){
            $theader .= '<td align="right" >'.$stay.'บ.</td>';
            $theader .= '
                 <td align="center" ><u> '.$cDatestart[2].'/'.$cDatestart[1].'/'.substr($cDatestart[0],2,4).'</u></td>';
            $theader .= '<td align="center" > - </td>';
          }else{
            $theader .= '<td align="right" ><u> '.$balances.' บ.</u></td>';

            $theader .= '
                 <td align="center" ><u> '.$cDatestart[2].'/'.$cDatestart[1].'/'.substr($cDatestart[0],2,4).'</u></td>';
             $theader .= '<td align="right" ><u> '.$row['account_paypertimes'].' บ.</u></td>';
          }
          $theader .= '<td><u> '.$row['account_type'].'</u></td>';
        }
        $theader .= '<td><u> '.$row['customer_nickname'].'</u></td>
             <td colspan="2"><u> '.$row['customer_address'].'</u></td>';
   }else{

        $theader .= '
             <td style="font-size:15px"> '.$account_name_new[0].' </td>
             ';
        if ($row['account_type'] == 'รายวัน') {


          $theader .= '<td align="right" >'.$row['account_outstanding'].' บ.</td>';
          $theader .= '<td align="right" >'.$balances.' บ.</td>';

          $theader .= '
                 <td align="center" > '.$cDatestart[2].'/'.$cDatestart[1].'/'.substr($cDatestart[0],2,4).'</td>';
           $theader .= '<td align="right" >'.$row['account_paypertimes'].' บ.</td>';
          $theader .= '<td> '.$row['account_duration'].' วัน </td>';

        }elseif ($row['account_type'] == 'ดอกลอย') {


          $theader .= '<td align="right" >'.$row['account_outstanding'].' บ.</td>';
          $theader .= '<td align="right" >'.$row['account_outstanding'].' บ.</td>';

          $theader .= '
                 <td align="center" > '.$cDatestart[2].'/'.$cDatestart[1].'/'.substr($cDatestart[0],2,4).'</td>';
          $theader .= '<td align="right" >'.$row['account_paypertimes'].' บ.</td>';
          $theader .= '<td> ดอกลอย </td>';

        }elseif ($row['account_type'] == 'รายเดือน'){


          $theader .= '<td align="right" >'.$row['account_outstanding'].' บ.</td>';
          if($row['account_type'] == 'รายเดือน'){
            $theader .= '<td align="right" >'.$stay.'บ.</td>';
            $theader .= '
                 <td align="center" > '.$cDatestart[2].'/'.$cDatestart[1].'/'.substr($cDatestart[0],2,4).'</td>';
            $theader .= '<td align="center" > - </td>';

          }else{
            $theader .= '<td align="right" >'.$balances.' บ.</td>';

            $theader .= '
                 <td align="center" > '.$cDatestart[2].'/'.$cDatestart[1].'/'.substr($cDatestart[0],2,4).'</td>';
            $theader .= '<td align="right" >'.$row['account_paypertimes'].' บ.</td>';

          }
           $theader .= '<td> '.$row['account_type'].'</td>';
        }
        $theader .= '<td> '.$row['customer_nickname'].'</td>
             <td colspan="2"> '.$row['customer_address'].'</td>';
   }


    $theader .='</tr>';
    $j++;
    //ห้ามลบเด้อ
    // if($j == 38 || $j == 75 || $j == 113 || $j == 151 || $j == 189 || $j == 227 || $j == 265 || $j == 303 || $j == 341 || $j == 379 || $j == 417 || $j == 455 || $j == 493){
    //   $theader .= '<tr class="blank_row">';
    //   $theader .= '<td style="border-right: white solid 1px;border-left: white solid 1px;border-bottom: white solid 1px;" height="20"  colspan="7"></td>';
    //   $theader .= '</tr>';
    // }
    // <td> '.$cDatestart[2].' / '.$cDatestart[1].' / '.$cDatestart[0].'</td>
}



mysqli_close($con);
$tfooter = '</table>';
// Print text using writeHTMLCell()
$pdf->writeHTML($theader.$tfooter, true, false, false, false, '');
// ---------------------------------------------------------
//Close and output PDF document
$date = date("d/m/Y");
list($d_o,$m_o,$Y_o) = split('/',$date);
$Y_o = $Y_o + 543;
$pdf->Output(''.$d_o.'-'.$m_o.'-'.$Y_o.'.pdf', 'I');

 ?>
